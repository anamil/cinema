<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/members', 'UsersController@index'); 
Route::get('members/create','UsersController@create');
Route::post('members/save','UsersController@store');
Route::get('members/{id}','UsersController@show');
Route::get('members/edit/{id}', 'UsersController@edit');
Route::patch('members/update/{id}', 'UsersController@update');
Route::delete('members/delete/{id}', 'UsersController@destroy');

Route::get('/movies', 'MoviesController@index');
Route::get('movies/create','MoviesController@create');
Route::post('movies/save','MoviesController@store');
Route::get('movies/{id}','MoviesController@show');
Route::get('movies/edit/{id}', 'MoviesController@edit');
Route::patch('movies/update/{id}', 'MoviesController@update');
Route::delete('movies/delete/{id}', 'MoviesController@destroy');


Route::get('/view-movies', 'ViewMoviesController@view_movies');
Route::get('/view-movies/{id}', 'ViewMoviesController@show_movie');

Route::get('/view-movies-theaters', 'ViewMoviesController@view_theaters');
Route::get('/view-movies-theaters/{id}', 'ViewMoviesController@show_theater');

Route::get('/view-movies-theaters/{id}/add-review', 'ViewMoviesController@create_review');
Route::post('/view-movies-theaters/save', 'ViewMoviesController@store_review');


Route::resource('/reviews', 'ReviewsController');
Route::get('reviews/create','ReviewsController@create');
Route::post('reviews/save','ReviewsController@store');
Route::get('reviews/{user_id}','ReviewsController@show');
Route::get('reviews/edit/{user_id}', 'ReviewsController@edit');
Route::patch('reviews/update/{user_id}', 'ReviewsController@update');
Route::delete('reviews/delete/{user_id}', 'ReviewsController@destroy');


Route::resource('/moviestheater', 'MoviestheaterController');
Route::get('moviestheater/create','MoviestheaterController@create');
Route::post('moviestheater/save','MoviestheaterController@store');
Route::get('moviestheater/{id}','MoviestheaterController@show');
Route::get('moviestheater/edit/{id}', 'MoviestheaterController@edit');
Route::patch('moviestheater/update/{id}', 'MoviestheaterController@update');
Route::delete('moviestheater/delete/{id}', 'MoviestheaterController@destroy');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
