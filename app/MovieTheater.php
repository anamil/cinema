<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MovieTheater extends Model
{

	protected $table ='movies_theater';

 	protected $fillable = ['title', 'capacity',  'hours'];
   
   	public $timestamps = false;

	public function movie()

	 {
	return $this->hasMany('\App\Movie', 'theater_id');
	  }
}

