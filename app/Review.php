<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model

   {
   	protected $primaryKey = 'user_id';
   	protected $table='reviews';
 	protected $fillable=['user_id', 'rating', 'title', 'description', 'movies_id'];
   
   	public $timestamps = false;

	}
   	
   

   