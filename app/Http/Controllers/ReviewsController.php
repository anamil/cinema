<?php

namespace App\Http\Controllers;
use App\Review;
use Session;

use Illuminate\Http\Request;


class ReviewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reviews = Review::all();
        //dd($members);
        return view('reviews.index',compact('reviews'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('reviews.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Review::create(request()->validate([
            'user_id'=>'required',
            'rating'=> 'required',
            'title'=>'required',
            'description'=>'required'
        ]));

        Session::flash('message','The item was added!');

        return redirect('/reviews');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($user_id)
    {
        return view('reviews.show', ['reviews' => reviews::findOrFail($id)]);
    }

    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($user_id)
    {
        $review = Review::findOrFail($user_id);
        //dd($review);
        return view('reviews.edit',compact('review'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($user_id)
    {
        request()->validate([
            'user_id' => 'required',
            'rating' => 'required',
            'title' => 'required',
            'description' => 'required'
        ]);

        $reviews->user_id = request('user_id');
        $reviews->rating = request('rating');
        $reviews->title = request('title');
        $members->description = request('description');

        $members->save();

        Session::flash('message','The item was updated!');

        return redirect('/reviews');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($user_id)
    {
        
        $review = Review::findOrFail($user_id);
        $review->delete();

        Session::flash('message','The item was deleted!');
        
        return redirect('/reviews');
    }
}
