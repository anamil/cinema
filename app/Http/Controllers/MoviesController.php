<?php

namespace App\Http\Controllers;

use App\Movie;

use Illuminate\Http\Request;

use Session;

class MoviesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $movies = Movie::all();
        //dd($members);
        return view('movies.index',compact('movies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('movies.create');
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    }    
    

     public function store(Request $request)
    {
        $request = request()->validate([
            'theater_id'=>'required|numeric',
            'title'=>'required',
            'release_date'=> 'required',
            'running_time'=>'required',
            'genre'=>'required',
            'cast'=>'required',
            'director'=>'required',
            'production_year'=>'required',
            'original_language'=>'required',
            'age_restrictions'=>'required|numeric|min:0',
            'price'=>'required|numeric',
            'trailer'=>'required',
        ]);

        Movie::create(request()->validate([
            'theater_id'=>'required',
            'title'=>'required',
            'release_date'=> 'required',
            'running_time'=>'required',
            'genre'=>'required',
            'cast'=>'required',
            'director'=>'required',
            'production_year'=>'required',
            'original_language'=>'required',
            'age_restrictions'=>'required|numeric|min:0',
            'price'=>'required',
            'trailer'=>'required', 
        ]));
            Session::flash('message','The item was added!');
            return redirect('/movies');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate
    */

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         return view('movies.show', ['movies' => Movie::findOrFail($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $movie = Movie::findOrFail($id);

    return view('movies.edit', compact('movie'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $movie)
    {
        // dd($request);

    
    request()->validate([
            'title'=>'required',
            'release_date'=> 'required',
            'running_time'=>'required',
            'genre'=>'required',
            'cast'=>'required',
            'director'=>'required',
            'production_year'=>'required',
            'original_language'=>'required',
            'age_restrictions'=>'required',
            // 'trailer'=>'required',
        ]);
            $movie->title = request('title');
            $movie->release_date = request('release_date');
            $movie->runing_time= request('runing_time');
            $movie->genre = request('genre');
            $movie->cast = request('cast');
            $movie->director = request('director');
            $movie->production_year = request('production_year');
            $movie->original_language = request('original_language');
            $movie->age_restrictions = request('age_restrictions');
            // $movies->trailer= request('trailer');

        $movie->save();
        Session::flash('message','The item was updated!');
        return redirect('/movies');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $movies = Movie::findOrFail($id);
        $movies->delete();

        Session::flash('message','The item was deleted!');
        return redirect('/movies');

    }
}
