<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Session;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $members = User::all();
        //dd($members);
        return view('members.index',compact('members'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('members.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         //($request->all());

         request()->validate([
            'name'=>'required',            
            'email' => 'required|unique:members',
            'password'=>'required|min:6|confirmed',
            'type'=>'required|in:admin,registered,visitor']);

        User::create($request->all());

        Session::flash('message','The item was added!');

            return redirect('/members');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
     return view('members.show', ['member' => User::findOrFail($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $member = User::findOrFail($id);
    return view('members.edit', ['member' => $member]);
    }
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $member = User::findOrFail($id);

        request()->validate([
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
            'confirm_password'=>'required',
            'age'=>'required',
            'type' => 'required',
        ]);

        $member->name = request('name');
        $member->email = request('email');
        $member->password = request('password');
        $member->confirm_password = request('confirm_password');
        $member->age = request('age');
        $member->type = request('type');

        $member->save();

        Session::flash('message','The item was updated!');

        return redirect('/members');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       
        $member = User::findOrFail($id);
        $member->delete();
        
        Session::flash('message','The item was deleted!');
        
        return redirect('/members');
    }
}