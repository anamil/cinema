<?php

namespace App\Http\Controllers;

use App\Movie;
use App\MovieTheater;
use App\Review;
use Illuminate\Http\Request;

class ViewMoviesController extends Controller
{
  public function view_movies()
  {
   $movies = Movie::all();  
   return view('visitmovies.visitMovies', compact('movies'));
  }

 public function show_movie($id)
 {
  $movie=Movie::findOrFail($id);
  $allReviews[$movie->id] = $movie->reviews_relations()->getResults();
  return view('visitmovies.showMovies', compact('movie','allReviews'));
  
  }

  public function view_theaters()
  {
   $moviestheaters = MovieTheater::all();
    foreach ($moviestheaters as $key => $movietheater) {
     $allMovies[$movietheater->id] = $movietheater->movie()->getResults();
    } 
   return view('visitmovies.visitTheaters', compact('moviestheaters','allMovies'));
  }

  public function show_theater($id)
  {
    $movietheater = MovieTheater::findOrFail($id);
    //dd($movietheater);
    $allMovies[$movietheater->id] = $movietheater->movie()->getResults();
    //dd($allMovies);
        foreach ($allMovies[$movietheater->id] as $key => $movie) {
           $allReviews[$movie->id] = $movie->reviews_relations()->getResults();
        }
        return view('visitmovies.showTheater', compact('movietheater', 'allMovies', 'allReviews'));
  }

  public function create_review($id)
  {
    $movie = Movie::findOrFail($id);
    //dd($movie);
    return view('visitmovies.addReview', compact('movie'));
  }

  public function store_review(Request $request)
  {
      request()->validate([
        'movies_id'=>'required',
        'title'=>'required',
        'raiting'=>'required|numeric',
        'description'=>'required'
        ]);

       Review::create(request()->validate([
            'movies_id'=>'required',
            'title'=>'required',
            'raiting'=>'required',
            'description'=>'required'
             
        ]));
            return redirect('/view-movies-theaters');
  }

}

