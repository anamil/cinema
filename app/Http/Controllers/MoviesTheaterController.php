<?php

namespace App\Http\Controllers;
use App\MovieTheater;
use Session;

use Illuminate\Http\Request;

class MoviesTheaterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $moviestheater = MovieTheater::all();
        //dd($members);
        return view('moviestheater.index',compact('moviestheater'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('moviestheater.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         MovieTheater::create(request()->validate([
            'title'=>'required',
            'capacity'=> 'required',
            'hours'=>'required|date_format:H:i',
           ]));

            Session::flash('message','The item was added!');
            return redirect('/moviestheater');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('moviestheater.show', ['movietheater' => MovieTheater::findOrFail($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $moviestheater = MovieTheater::findOrFail($id);

        return view('moviestheater.edit')->withMovieTheater($moviestheater);
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        request()->validate([
            'name'=>'required',
            'capacity'=> 'required',
            'hours'=>'required',
        ]);
            $moviestheater->title = request('title');
            $moviestheater->capacity = request('capacity');
            $moviestheater->hours= request('hours');
            
        $moviestheater->save();

        Session::flash('message','The item was updated!');

        return redirect('/moviestheater');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $moviestheater = MovieTheater::findOrFail($id);
        $moviestheater->delete();

        Session::flash('message','The item was deleted!');
        return redirect('/moviestheater');
    
    }
}
