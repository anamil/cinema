<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
	protected $table ='movies';

 	protected $fillable = ['title', 'release_date', 'running_time', 'genre', 'cast', 'director', 'production_year', 'original_language', 'age_restrictions', 'price', 'theater_id', 'trailer'];
   
   	public $timestamps = false;

   	 public function reviews_relations()
	{
		return $this->hasMany('\App\Review', 'movies_id');
	}

}