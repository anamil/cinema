@extends('layouts.front')

@section('title')
	Reviews
@endsection

@section('content')
<div class = "card">
	@if ($errors->any())
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
			<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
	@endif
	<h1>Create a review</h1>
	<form method="post" action="/reviews">
		{{ csrf_field() }}
		<div class="row">
			<div class="col-4">
				<label for="text">User Id</label>
			</div>
			<div class = "col-8">
				<input type="text" name ="user_id"  id ="user_id"  value="<?php echo old('user_id');?>">
			</div>
		</div>
		<div class="row">
			<div class="col-4">
				<label for="Text">Rating</label>
			</div>
			<div class= "col-8">
				<input type="text" id ="rating" name="rating">
			</div>
		</div>
		<div class="row">
			<div class="col-4">
				<label for="text">Title</label>
			</div>
			<div class="col-8">
				<input type="text" id="title" name="title">
			</div>
		</div>
		<div class="description">
			<div class="col-4">
				<p>Description</p>
				<textarea name="description"></textarea>
			</div>
		</div>
		<div class="col-4">
			<button type="submit">Create a reviews</button>
		</div>
	</form>
</div>
@endsection
