@extends('layouts.front')

@section('title')
	Reviews
@endsection

@section('content')
	<div class = "container bg-light p-5 my-5 border border-info rounded-sm">
    <div class="d-flex justify-content-center display-4 mb-5">Reviews</div>
  </div>
	<div class="container border border-info">
		<table class="table table-striped table-hover p-5 my-5 border border-info">
			<tr class="table-dark">
				<th>User Id</th>
				<th>Rating</th>
				<th>Title</th>
				<th>Description</th>
				<th>Actions</th>
			</tr>	
			@foreach($reviews as $review)
			<tr>
				<td>{{$review->user_id}}</td>
				<td>{{$review->rating}}</td>
				<td>{{$review->title}}</td>
				<td>{{$review->description}}</td>
				<td>
					<a href="/reviews/edit/{{$review->user_id}}">Edit</a>
					<form method="post" action="/reviews/delete/<?php echo $review->user_id;?>">
              <?php echo method_field('Delete');?>
              <?php echo csrf_field(); ?>
              <button type="submit" class ='btn btn-primary m-1'>Delete</button>
            </form>
					<!-- <a href="/reviews/delete/{{$review->user_id}}">Delete</a> -->
				</td>
			</tr>
			@endforeach
		</table>
		<a href="/reviews/create">Add a review</a>
	</div>
	@if(Session::has('message'))
      {{Session::get('message')}}
    @endif
@endsection
	