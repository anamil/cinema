@extends('layouts.front')

@section('title')
 Visit Movies
@endsection

@section('content')


<div class='row'>
  @foreach($movies as $movie)
  <div class="col-4 border border-info px-2">
  	<div class="embed-responsive embed-responsive-4by3">
    {!! htmlspecialchars_decode($movie->trailer, ENT_NOQUOTES) !!} 
    </div>
     <div class="col-sm">

      <a href="/view-movies/{{$movie->id}}" class = 'btn btn-primary m-1'>{{$movie->title}}</a>
      </div>
    
  </div>
@endforeach
</div>
@endsection