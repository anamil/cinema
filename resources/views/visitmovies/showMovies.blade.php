<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<title>Movies</title>
</head>
	<body>
    <div class="container">
<div class="row">
  <div class="col-5 text-dark">
    <h5 class="text-left text-primary">Details about movie</h5><br>
    <div class='row mb-1'>
      <div class='col-5'>Title:</div>
      <div class='col-7'>{{$movie->title}}</div>
    </div>
    <div class='row mb-1'>
      <div class='col-5'>Release date:</div>
      <div class='col-7'>{{$movie->release_date}}</div>
    </div>
    <div class='row mb-1'>
      <div class='col-5'>Runnimg time:</div>
      <div class='col-7'>{{$movie->running_time}} min</div>
    </div>
    <div class='row mb-1'>
      <div class='col-5'>Genre:</div>
      <div class='col-7'>{{$movie->genre}}</div>
    </div>
    <div class='row mb-1'>
      <div class='col-5'>Cast:</div>
      <div class='col-7'>{{$movie->cast}}</div>
    </div>
    <div class='row mb-1'>
      <div class='col-5'>Director:</div>
      <div class='col-7'>{{$movie->director}}</div>
    </div>
    <div class='row mb-1'>
      <div class='col-5'>Production:</div>
      <div class='col-7'>{{$movie->production}}</div>
    </div>
    <div class='row mb-1'>
      <div class='col-5'>Original language:</div>
      <div class='col-7'>{{$movie->language}}</div>
    </div>
    <div class='row mb-1'>
      <div class='col-5'>Age restrction:</div>
      <div class='col-7'>{{$movie->age_restrictions}}</div>
    </div>
    <hr> 
  </div>
  <div class="col-7">
    <div class="embed-responsive embed-responsive-4by3 m-4" style="margin-right: 150px;">
      {!! htmlspecialchars_decode($movie->trailer, ENT_NOQUOTES) !!} 
    </div>
  </div>
  </div>

  <div class="col-12 text-dark">
    @foreach($allReviews[$movie->id] as $review)
      <div class='row mb-1'>
        <div class='col-5'>Title:</div>
        <div class='col-7'>{{$review->title}}</div>
    </div>
    <div class='row mb-1'>
        <div class='col-5'>Raiting:</div>
        <div class='col-7'>{{$review->raiting}}</div>
    </div>
    <div class='row mb-1'>
        <div class='col-5'>Description:</div>
        <div class='col-7'>{{$review->description}}</div>
    </div>
    @endforeach
  </div>
</div>
	</body>
</html>
	