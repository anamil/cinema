@extends('layouts.front')

@section('title')
 Visit Theater
@endsection

@section('content')


<div class='row'>
  @foreach($moviestheaters as $movietheater)
  <div class="col-4 border border-info px-2">
  	
     <div class="col-sm">

      <a href="/view-movies-theaters/{{$movietheater->id}}" class = 'btn btn-primary m-1'>{{$movietheater->title}}</a>
      
  @foreach($allMovies[$movietheater->id] as $key => $movie)
  	{{ $movie->title }}<br>
  @endforeach
      </div>
    
  </div>
@endforeach
</div>
@endsection