@extends('layouts.front')

@section('title')
Show Theater
@endsection

@section('content')
<div class="row">
<div class=" col-5">
  @foreach($allMovies[$movietheater->id] as $movie)  
  <h3 class="text-left text-primary">Details about movie</h3><br>
  <div class='row mb-1'>
    <div class='col-5'>Title:</div>
    <div class='col-7'>{{$movie->title}}</div>
  </div>
  <div class='row mb-1'>
    <div class='col-5'>Release date:</div>
    <div class='col-7'>{{$movie->release_date}}</div>
  </div>
  <div class='row mb-1'>
    <div class='col-5'>Running time:</div>
    <div class='col-7'>{{$movie->running_time}}</div>
  </div>
  <div class='row mb-1'>
    <div class='col-5'>Genre:</div>
    <div class='col-7'>{{$movie->genre}}</div>
  </div>
  <div class='row mb-1'>
    <div class='col-5'>Cast:</div>
    <div class='col-7'>{{$movie->cast}}</div>
  </div>
  <div class='row mb-1'>
    <div class='col-5'>Director:</div>
    <div class='col-7'>{{$movie->director}}</div>
  </div>
  <div class='row mb-1'>
    <div class='col-5'>Production:</div>
    <div class='col-7'>{{$movie->production_year}}</div>
  </div>
  <div class='row mb-1'>
    <div class='col-5'>Original language:</div>
    <div class='col-7'>{{$movie->original_language}}</div>
  </div>
  <div class='row mb-1'>
    <div class='col-5'>Age restrction:</div>
    <div class='col-7'>{{$movie->age_restriction}}</div>
  </div>
  @endforeach
</div>
<div class="col-7">
  <div class="embed-responsive embed-responsive-4by3">
    {!! htmlspecialchars_decode($movie->trailer, ENT_NOQUOTES) !!} 
  </div>
</div>

<br><br>
<div class="col-12">
  <div class='row mb-2'>
    <div class='col-5'><h3 class="text-left text-primary">Reviews</h3></div> 
    <div class='col-7'>
      <a href="/view-movies-theaters/{{$movie->id}}/add-review" class = 'btn btn-primary m-1'>Give a review to movie</a>
    </div>
  </div>
  </br>
  @if(isset($allReviews) && empty($allReviews))
    {{ $message = "Still no reviews for this movie." }}
  @else
  @foreach($allReviews[$movie->id] as $review)
  <div class='row mb-1'>
    <div class='col-5'>Title:</div>
    <div class='col-7'>{{ $review->title }}</div>
  </div>
  <div class='row mb-1'>
    <div class='col-5'>Rating:</div>
    <div class='col-7'>{{ $review->rating }}</div>
  </div>
  <div class='row mb-1'>
    <div class='col-5'>Description:</div>
    <div class='col-7'>{{ $review->description }}</div>
  </div>
  <hr> 
  @endforeach
  @endif
</div>
@endsection