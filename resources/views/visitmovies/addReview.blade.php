@extends('layouts.front')

@section('title')
Add a review
@endsection

@section('content')

<h1>My review</h1>
	<form method="post" action="/view-movies-theaters/">
		{{ csrf_field() }}
				<input type="hidden" id="movies_id" name="movies_id" value="$movie->id">
		<div class="row">
			<div class="col-4">
				<label for="text">Title</label>
			</div>
			<div class = "col-8">
				<input type="text" name ="text"  id ="title">
			</div>
		</div>
		<div class="row">
			<div class="col-4">
				<label for="Text">Rating</label>
			</div>
			<div class= "col-8">
				<input type="text" id ="rating" name="rating">
			</div>
		</div>
		<div class="description">
			<div class="col-4">
				<p>Description</p>
				<textarea name="description"></textarea>
			</div>
		</div>
		<div class="col-4">
			<button type="submit">Add review</button>
		</div>
	</form>
</div>
@endsection