<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<title>@yield('title')</title>
</head>
	<body>
	<div class = "d-flex justify-content-end p-5">
		<a href="{{ url('/') }}">Home</a>
	</div>
	<ul class="nav justify-content-end">
		<li class="nav-item">
			<a class="nav-link" href="/members">Members</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" href="/movies">Movies</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" href="/moviestheater">Movies Theater</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" href="/reviews">Reviews</a>
		</li>
	</ul>
		<div class="container">
			@yield('content')
		</div>
	</body>
</html>