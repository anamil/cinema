@extends('layouts.front')

@section('title')
	Members
@endsection

@section('content')
<div class = "card">
	@if ($errors->any())
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
			<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
	@endif
	<h1>Create a member</h1>
	<form method="post" action="/members/save">
		{{ csrf_field() }}
		<div class="row">
			<div class="col-4">
				<label for="name">Name</label>
			</div>
			<div class = "col-8">
				<input type="text" name ="name"  id ="name"  value="<?php echo old('name');?>">
			</div>
		</div>
		<div class="row">
			<div class="col-4">
				<label for="email">Email</label>
			</div>
			<div class= "col-8">
				<input type="email" id ="email" name="email">
			</div>
		</div>
		<div class="row">
			<div class="col-4">
				<label for="password">Password</label>
			</div>
			<div class="col-8">
				<input type="password" id="password" name="password">
			</div>
		</div>
		<div class="row">
			<div class="col-4">
				<label for="password">Password Confirmation</label>
			</div>
			<div class="col-8">
				<input type="password" id="password_confirmation" name="password_confirmation">
			</div>
		</div>
		<div class="row">
			<div class="col-4">
				<label for="type" id="type" name="type">Type</label>
			</div>
			<div class="col-8">
				<input type="radio" name="type" value="visitor">Visitor<br>
				<input type="radio" name="type" value="registered">Register<br>
				<input type="radio" name="type" value="admin">Admin
			</div>
		</div>
		<div class="col-4">
			<button type="submit">Create a member</button>
		</div>
	</form>
</div>
@endsection