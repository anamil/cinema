@extends('layouts.front')

@section('title')
		Members
@endsection

@section('content')
	<div class = "container bg-light p-5 my-5 border border-info rounded-sm">
		<div class="d-flex justify-content-center display-4 mb-5">Members</div>
	</div>
	<div class="container border border-info">
		<table class="table table-striped table-hover p-5 my-5 border border-info">
			<tr class="table-dark">
				<th>Name</th>
				<th>Email</th>
				<th>Password</th>
				<th>Type</th>
				<th>Actions</th>
			</tr>	
			@foreach($members as $member)
			<tr>
				<td>{{$member->name}}</td>
				<td>{{$member->email}}</td>
				<td>{{$member->password}}</td>
				<td>{{$member->type}}</td>
				<td>
					<a href="/members/edit/{{$member->id}}">Edit</a>
					<a href="/members/edit/{{$member->id}}">Delete</a>
				</td>
			</tr>
			@endforeach
		</table>
	</div>
	<br>
	<div class="d-flex justify-content-center">
		@if(Session::has('message'))
     	 {{Session::get('message')}}
    	@endif
	</div>
@endsection