@extends('layouts.front')

@section('title')
	Movie Theater
@endsection

@section('content')
<div class = "card">
	@if ($errors->any())
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
			<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
	@endif
	<h1>Create a movietheater</h1>
	<form method="post" action="/moviestheater/save">
		{{ csrf_field() }}
		<div class="row">
			<div class="col-4">
				<label for="name">Title</label>
			</div>
			<div class = "col-8">
				<input type="text" name ="title"  id ="title"  value="<?php echo old('title');?>">
			</div>
		</div>
		<div class="row">
			<div class="col-4">
				<label for="capacity">Capacity</label>
			</div>
			<div class= "col-8">
				<input type="text" id ="capacity" name="capacity">
			</div>
		</div>
		<div class="row">
			<div class="col-4">
				<label for="text">Hours</label>
			</div>
			<div class="col-8">
				<input type="text" id="hours" name="hours">
			</div>
		</div>
		<div>
			<div class="row">
				<div class="col-4">
					<label for="type" id="type" name="type">Type</label>
				</div>
				<div class="col-8">
					<input type="radio" name="type" value="visitor"> Visitor<br>
					<input type="radio" name="type" value="Registered users"> Registered users<br>
					<input type="radio" name="type" value="admin">Admin
				</div>
				<div class="col-4">
					<button type="submit">Create a movie theater</button>
				</div>
			</form>
		</div>
@endsection
