@extends('layouts.front')

@section('title')
	Movies Theater
@endsection

@section('content')
	<div class = "container bg-light p-5 my-5 border border-info rounded-sm">
    <div class="d-flex justify-content-center display-4 mb-5">Movies theater</div>
  </div>
	<div class="container border border-info">
		<table class="table table-striped table-hover p-5 my-5 border border-info">
			<tr class="table-dark">
				<th>Title</th>
				<th>Hours</th>
				<th>Capacity</th>
				<th>Actions</th>
			</tr>	
			@foreach($moviestheater as $movie_theater)
			<tr>
				<td>{{$movie_theater->title}}</td>
				<td>{{$movie_theater->hours}}</td>
				<td>{{$movie_theater->capacity}}</td>
				<td>
					<a href="/moviestheater/edit/{{$movie_theater->id}}">Edit</a>
					<a href="/moviestheater/edit/{{$movie_theater->id}}">Delete</a>
				</td>
			</tr>
			@endforeach
		</table>
		<a href="/movies/create" class="btn btn-primary">Add a new movie</a>	
	</div>
	<br>
	<div class="d-flex justify-content-center">
		@if(Session::has('message'))
     	 {{Session::get('message')}}
    	@endif
	</div>
@endsection
	