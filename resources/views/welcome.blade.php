<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

        <title></title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background: black;
                color: #636b6f;
                font-family: 'Arial', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 22px;
                top: 20px;
                margin: 0px 0px 0px 950px;
                }
            
            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: black;
                padding: 2px 13px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
                border-radius: 30px;
                background: #f47b0a;
            }

            .m-b-md {
                margin-bottom: 30px;
                color:#f47b0a;
            }

            .logo{
                height: auto;
                position: fixed;
                top: 20px;
                left: 140px;
                font-size: 18px;     
            }


            }
        .movie_slider{
              position: relative;
                    }
        .movie_slider .container{
        width: 100%;
        max-width: 1160px;
        padding: 0;
        min-height: 708px;
        position: absolute;
        z-index: 2;
        top: 0;
        left: 50%;
        -webkit-transform: translateX(-50%);
        transform: translateX(-50%);
    }
        

    </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                     <img src="1.jpg" width="120" class="logo">
                    @auth
                        <a href="{{ url('home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif
            <div class="main-menu">
        <div class="content"> 
                <div class="title m-b-md">
                   Cinema city
                </div>
         <div class="main-menu">
                    <div class="container-fluid">
                <div class="links">
                    <a href="#">Program</a>
                    <a href="{{ url('/view-movies') }}">Movies</a>
                    <a href="{{ url('/view-movies-theaters') }}">Movies Theater</a>
                    <a href="#">Booking</a>
                    <a href="{{ url('/reviews') }}">Reviews</a>
                    <a href="#">Contact</a>
                </div>
            </div>
        </div>
      </div> 


    <div class="cinema_slider">
        <h2></h2>
     <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="item active">
        <img src="img1.jpg" style="width:auto"/>
      </div>

      <div class="item">
        <img src="img5.jpg" style="width:auto"/>
      </div>
    
      <div class="item">
        <img src="img7.jpg" style="width:auto"/>
      </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>
<section class="dark footer">





    


</body>
</html>

           
    </body>
</html>
