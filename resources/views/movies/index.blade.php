@extends('layouts.front')

@section('title')
Movies
@endsection

@section('content')
<div class = "container"> 
	<div class="d-flex justify-content-center display-4 mb-4">Movies</div> 
</div>
<div class="container border border-info">
	<table class="table table-striped table-hover border border-info">
		<tr class="table-dark">
			<th class="col-sm-1">Title</th>
			<th class="col-sm-1">Release date</th>
			<th class="col-sm-1">Running time</th>
			<th class="col-sm-1">Cast</th>
			<th class="col-sm-1">Director</th>
			<th class="col-sm-1">Production Year</th>
			<th class="col-sm-1">Original language</th>
			<th class="col-sm-1">Age restriction</th>
			<th class="col-sm-1">Price</th>
			<th class="col-sm-2">Trailer</th>
			<th class="col-sm-1">Actions</th>


		</tr>	
		@foreach($movies as $movie)
		<tr>
			<td>{{$movie->title}}</td>
			<td>{{$movie->release_date}}</td>
			<td>{{$movie->cast}}</td>
			<td>{{$movie->running_time}}</td>
			<td>{{$movie->director}}</td>
			<td>{{$movie->production_year}}</td>
			<td>{{$movie->age_restriction}}</td>
			<td>{{$movie->original_language}}</td>
			<td>{{$movie->price}}</td>
			<td>
				@if(strlen($movie->trailer) > 12)
				{{ str_pad(substr($movie->trailer,0,12),15,".") }}
				@else
				{{$movie->trailer}}
				@endif
			</td>

			<td>{{$movie->actions}}</td>
			<td>
				<a href="/movies/edit/{{$movie->id}}" class="btn btn-primary">Edit</a>
				<div class="col-sm">
					<form method="post" action="/movies/delete/{{$movie->id}}">
						<?php echo method_field('Delete'); ?>
						<?php echo csrf_field(); ?>
						<button type="submit" class ='btn btn-primary'>Delete</button>
					</form>
				</div>
			</tr>
			@endforeach 
		</table>
		<a href="/movies/create" class="btn btn-primary">Add a new movie</a>	
	</div>
	<br>
	<div class="d-flex justify-content-center">
		@if(Session::has('message'))
     	 {{Session::get('message')}}
    	@endif
	</div>
	@endsection
