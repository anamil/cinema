@extends('layouts.front')

@section('title')
Movies
@endsection

@section('content')<div class = "card">
	@if ($errors->any())
<div class="alert alert-danger">
	<ul>
		@foreach ($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
</div>
	@endif
<h1>Create a movie</h1>
	<form method="post" action="/movies/save">
		{{ csrf_field() }}
		<div class="row">
			<div class="col-4">
				<label for="text">Movie Theater Id</label>
			</div>
			<div class = "col-8">
				<input type="text" name ="theater_id"  id ="theater_id">
			</div>
		</div>
		<div class="row">
			<div class="col-4">
				<label for="text">Title</label>
			</div>
			<div class = "col-8">
				<input type="text" name ="title"  id ="text">
			</div>
		</div>

		<div class="row">
			<div class="col-4">
				<label for="release date">Release date</label>
			</div>
			<div class= "col-8">
				<input type="text" id ="release_date" name="release_date" placeholder="dd/mm/yyyy">
			</div>
		</div>

		<div class="row">
			<div class="col-4">
				<label for="text">Running time</label>
			</div>
			<div class="col-8">
				<input type="text" id="running_time" name="running_time">
			</div>
		</div>

		<div class="row">
			<div class="col-4">
				<label for="text">Genre</label>
			</div>
			<div class="col-8">
				<input type="text" id="genre" name="genre">
			</div>
		</div>

		<div class="row">
			<div class="col-4">
			<label for="text">Cast</label>
			</div>
			<div class="col-8">
				<input type="text" id="cast" name="cast">
			</div>
		</div>

		<div class="row">
			<div class="col-4">
				<label for="text">Director</label>
			</div>
			<div class="col-8">
				<input type="text" id="director" name="director">
			</div>
		</div>

		<div class="row">
			<div class="col-4">
				<label for="production_year">Production Year</label>
			</div>
			<div class= "col-8">
				<input type="text" id ="production_year" name="production_year">
			</div>
		</div>

		<div class="row">
			<div class="col-4">
				<label for="text">Original language</label>
			</div>
			<div class= "col-8">
				<input type="text" id ="original_language" name="original_language">
			</div>
		</div>

		<div class="row">
			<div class="col-4">
				<label for="age_restrict">Age restriction</label>
			</div>
			<div class= "col-8">
				<select name="age_restrictions">
					<option value="0">0</option>
					<option value="18">18</option>
				</select>	
			</div>
		</div>

		<div class="row">
			<div class="col-4">
			<label for="text">Price</label>
			</div>
			<div class= "col-8">
				<input type="text" id ="price" name="price">
			</div>
		</div>
		<div class="row">
			<div class="col-4">
			<label for="text">Trailer</label>
			</div>
			<div class= "col-8">
				<input type="text" id ="trailer" name="trailer">
			</div>
		</div>
	<div class="col-4">
		<button type="submit">Create a movie</button>
	</form>
	</div>
@endsection
