
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>

	<h1>Edit Movies</h1>
	<form method="GET" action="/movies/edit/<?php echo $movie->id; ?>">
	<?php 
	echo csrf_field();
	echo method_field('PATCH');
	?>

		<div>
			<input type="text" name="id" value="<?php echo $movie->id;?>">
		</div>
		<div>
			<input type="text" name="title" value="<?php echo $movie->title;?>">
		</div>
		<div>
			<input type="release_date" name="release_date" value="<?php echo $movie->release_date;?>">
		</div>
		<div>
			<input type="running_time" name="running_time" value="<?php echo $movie->running_time;?>">
		</div>
		<div>
			<input type="genre" name="genre" value="<?php echo $movie->genre;?>">
		</div>
		<div>
			<input type="cast" name="cast" value="<?php echo $movie->cast;?>">
		</div>
		</div>
		<div>
			<input type="director" name="director" value="<?php echo $movie->director;?>">
		</div>
		</div>
		<div>
			<input type="production_year" name="production_year" value="<?php echo $movie->production_year;?>">
		</div>
		</div>
		<div>
			<input type="original_language" name="original_language" value="<?php echo $movie->original_language;?>">
		</div>
		</div>
		<div>
			<input type="age_restrict" name="age_restriction"value="<?php echo $movie->age_restriction;?>">
		</div>
		</div>
		<div>
			<input type="price" name="price"value="<?php echo $movie->price;?>">
		</div>
		</div>
	
		<div>
			<button type="submit">Update movie</button> 
		</div>
	</form>
	<form action="POST" action="/movies/<?php echo $movie->id;?>">
		<?php echo method_field('Delete');?>
		<?php echo csrf_field(); ?>
		<button type="submit">Delete movie</button>
	</form>