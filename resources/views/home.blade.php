@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <!-- <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>


        </div> -->

        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                     <img src="1.jpg" width="120" class="logo">
                    @auth
                        <a href="{{ url('home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif
            <div class="main-menu">
        <div class="content"> 
                <div class="title m-b-md">
                   Cinema city
                </div>
         <div class="main-menu">
                    <div class="container-fluid">
                <div class="links">
                    <a href="#">Program</a>
                    @if(Auth::user()->type && Auth::user()->type == 'Visitor') 
                    <a href="{{ url('/view-movies') }}">Movies</a>
                    @endif
                    @if(Auth::user()->type && Auth::user()->type == 'Admin')  
                    <a href="{{ url('/members') }}">Admin</a>
                    @endif
                    @if(Auth::user()->type && Auth::user()->type == 'Registered')  
                    <a href="{{ url('/view-movies-theaters') }}">Movies Theater</a>
                    @endif
                    <a href="#">Booking</a>
                    <a href="{{ url('/reviews') }}">Reviews</a>
                    <a href="#">Contact</a>
                </div>
            </div>
        </div>
      </div> 


    <div class="cinema_slider">
        <h2></h2>
     <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="item active">
        <img src="img1.jpg" style="width:auto"/>
      </div>

      <div class="item">
        <img src="img5.jpg" style="width:auto"/>
      </div>
    
      <div class="item">
        <img src="img7.jpg" style="width:auto"/>
      </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>
<section class="dark footer">
<div class="text-white">
     
  </div>
 @if(Session::has('type'))
      {{Session::get('type')}}
    @endif
    @if(Session::has('age'))
      {{Session::get('age')}}
    @endif
    </div>

</div>
@endsection
